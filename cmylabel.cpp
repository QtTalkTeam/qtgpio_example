#include <QPixmap>
#include <QMouseEvent>

#include "cmylabel.h"

cMyLabel::cMyLabel(QWidget *parent) : QLabel(parent)
{

    status = false;
    setImage();

}

void cMyLabel::mousePressEvent(QMouseEvent* ev){

    if(ev->MouseButtonPress == Qt::RightButton){
        status = !status;
        setImage();
        emit onClickSignal(status);
    }
}

void cMyLabel::setImage(){

    if(status == false){
        this->setPixmap(QPixmap(":/new/prefix1/img/twiz-light-bulb-unlit.png"));
    }else
        this->setPixmap(QPixmap(":/new/prefix1/img/twiz-light-bulb-unlit_on.png"));

    this->update();
}
