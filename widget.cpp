#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QFile>
#include <gpio.h>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    //
    // Set StyleSheet for application
    //
    QFile file(":/new/prefix1/img/appCss.txt");
    file.open(QFile::ReadOnly);
    QString mStyleSheet = QLatin1String(file.readAll());
    this->setStyleSheet(mStyleSheet);

    //
    // Connect Label signals to their slots
    //
    connect(ui->rel_1,SIGNAL(onClickSignal(bool)),this,SLOT(rel1Slot(bool)));
    connect(ui->rel_2,SIGNAL(onClickSignal(bool)),this,SLOT(rel2Slot(bool)));

    //
    // Create a Gpio pins to control relay.
    //
    pinRelay1 = new GPIO(1,2);
    pinRelay2 = new GPIO(2,11);

    pinRelay1->openPin();
    pinRelay2->openPin();
    pinRelay1->setDirection(GPIO::Out);
    pinRelay2->setDirection(GPIO::Out);


    pGPIOManager = new GPIOManager(this);
    connect(pGPIOManager, SIGNAL(onInputPinStatusChanged(GPIO*, bool)), this, SLOT(inputPinsStatusChangedSlot(GPIO*, bool)));

    //Out Pins
    pinRelay1 = pGPIOManager->addPin(1,2);
    pinRelay2 = pGPIOManager->addPin(2,11);


    //Input Pins
    pGPIOManager->addPin(1,4,GPIO::In);
    pGPIOManager->addPin(3,13,GPIO::In);

    pGPIOManager->startService();

}

Widget::~Widget()
{
    delete ui;
}

void Widget::rel1Slot(bool stat){
    pinRelay1->setState(stat);

}
void Widget::rel2Slot(bool stat){
    pinRelay2->setState(stat);
}

void Widget::inputPinsStatusChangedSlot(GPIO* p, bool stat){
    qDebug() << QString("Pin %1 cambia stato: %2").arg(p->getAbsPinNumber()).arg(stat);
}
