#ifndef CMYLABEL_H
#define CMYLABEL_H

#include <QLabel>

class cMyLabel : public QLabel
{
    Q_OBJECT

private:
    bool status;
    void setImage();

protected:
    void mousePressEvent(QMouseEvent* ev);

public:
    explicit cMyLabel(QWidget *parent = 0);

    bool getStatus(){
     return(status);
    }

    void setStatus(bool onoff){
        status = onoff;
        setImage();
    }

signals:
        void onClickSignal(bool);

public slots:
};

#endif // CMYLABEL_H
