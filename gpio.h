#ifndef GPIO_H
#define GPIO_H

#include <qstring.h>
#include <qobject.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <QThread>
#include <QVector>
#include <QHash>


class GPIO : public QObject
{
    Q_OBJECT

public:    

    enum Direction {In,Out,Err};

    GPIO(int port, int pin, QObject* parent = NULL);
    GPIO(int absPin, QObject* parent = NULL);

    ~GPIO();

    //To export the pin into the system
    int openPin();

    //To close all pin resources
    int closePin();

    //Set direction in/out.
    int setDirection(Direction direction);

    //returns direction
    Direction getDirection();

    bool isInput(){
        return(getDirection() == GPIO::In);
    }

    int setState(bool state);
    void setValue(bool value);
    bool getState();

    int getAbsPinNumber(){
        return(_pinNumber);
    }

private:
    //Private members
    Direction _direction;
    bool    _state;
    int     _pinNumber;
    QString _directionString;
    QString _valueString;
    QString _strPin;
};

class GPIOManager;
//----------------------------------------------------------------------------
class TInputPinThread : public QThread
{

    Q_OBJECT

private:
    GPIOManager* pOwner;
    int nMs;

    volatile bool _terminate;
    volatile bool _isRunning;
    QThread::Priority _priority;

public:
    explicit TInputPinThread(GPIOManager* powner, int nms=250);

    bool seTerminate(){
        return(_terminate);
    }

    bool isRunning(){
        return(_isRunning);
    }

    void Start(int ms);
    void Stop();

protected:
    // overriding the QThread's run() method
    void run();


};

//----------------------------------------------------------------------------
class GPIOManager : public QObject
{

    Q_OBJECT

private:
    TInputPinThread* pInputThread;

    QVector<GPIO *>     gpio;
    QVector<bool>       oldGpioState;

    QHash<int, GPIO*>   gpioHashMap;

    int getAbsPinNumber(int port, int index){
        return((((port)-1)*32)+((index)&31));
    }

    bool checkPortPin(int port, int pin);
    bool checkPortPin(int absolutePinNumber);

public:

    GPIOManager(QObject* parent = NULL);
    ~GPIOManager();

    //Create a new one pin
    GPIO* addPin(int port, int pin, GPIO::Direction dir = GPIO::Out);

    GPIO* getGPIOPointer(int port, int index){
        return(gpioHashMap.value((((port)-1)*32)+((index)&31)));
    }

    GPIO* getGPIOPointer(int absIdx){
        return(gpioHashMap.value(absIdx));
    }

    void startService(int nms = 250);

    void ckeckInputPin();

signals:
    void onInputPinStatusChanged(GPIO*, bool);


};
#endif // GPIO_H
