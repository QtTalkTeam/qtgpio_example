#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class GPIO;
class GPIOManager;

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;

    GPIO* pinRelay1;
    GPIO* pinRelay2;


    GPIOManager* pGPIOManager;

private slots:
    void rel1Slot(bool);
    void rel2Slot(bool);

    void inputPinsStatusChangedSlot(GPIO*, bool stat);
};

#endif // WIDGET_H
