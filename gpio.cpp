#include <QDebug>
#include "gpio.h"

//------------------------------------------------------------------------------
GPIO::GPIO(int port, int index, QObject* parent) : QObject(parent) {

    //toDo: Check port and Index please !!

    _pinNumber = (((port)-1)*32)+((index)&31);

    _valueString        = QString("/sys/class/gpio/gpio%1/value").arg(_pinNumber);
    _directionString    = QString("/sys/class/gpio/gpio%1/direction").arg(_pinNumber);
    _strPin             = QString("%1").arg(_pinNumber);

    _state              = false;

#ifdef _debug
    qDebug() << "_pinNumber :" << _pinNumber;
    qDebug() << "_valueString :" << _valueString;
    qDebug() << "_directionString :" << _directionString;
    qDebug() << "_strPin :" << _strPin;
#endif
}
//------------------------------------------------------------------------------
GPIO::GPIO(int pin, QObject* parent) : QObject(parent) {

    //toDo: Check the reange please !

    _pinNumber          = pin;

    _valueString        = QString("/sys/class/gpio/gpio%1/value").arg(_pinNumber);
    _directionString    = QString("/sys/class/gpio/gpio%1/direction").arg(_pinNumber);
    _strPin             = QString("%1").arg(_pinNumber);

    _state              = false;
}
//------------------------------------------------------------------------------
GPIO::~GPIO()
{
    FILE * fp;

    if ((fp = fopen("/sys/class/gpio/unexport", "ab")) == NULL){
        return;
    }

    rewind(fp); //Set pointer to begining of the file

    if(fwrite(_strPin.toLatin1(), sizeof(char), _strPin.length(), fp) != sizeof(char)){
        qDebug() << "write error";
    }

    fclose(fp);
}
//------------------------------------------------------------------------------
int GPIO::openPin()
{
    FILE * fp;

    if ((fp = fopen("/sys/class/gpio/export", "ab")) == NULL){
        return -1;
    }
#ifdef _debug
    qDebug() << "echo " << _strPin << " > /sys/class/gpio/export";
#endif
    rewind(fp);//Set pointer to begining of the file
    fwrite(_strPin.toLatin1(), sizeof(char),_strPin.length() , fp);

    fclose(fp);
    return 0;
}
//------------------------------------------------------------------------------
int GPIO::closePin()
{
    FILE * fp;

    if ((fp = fopen("/sys/class/gpio/unexport", "ab")) == NULL)
        return -1;
#ifdef _debug
    qDebug() << "echo " << _strPin << " > /sys/class/gpio/unexport";
#endif
    rewind(fp);//Set pointer to begining of the file
    fwrite(_strPin.toLatin1(), sizeof(char),_strPin.length(), fp);

    fclose(fp);
    return 0;
}
//------------------------------------------------------------------------------
int GPIO::setDirection(Direction direction)
{
    //set direction in/out.
    //returns 0 if ok and -1 on error

    FILE * fp;

    if ((fp = fopen(_directionString.toLatin1(), "rb+")) == NULL){
        return -1;
    }

    rewind(fp);//Set pointer to begining of the file


    if(direction == In){
        fwrite("in", sizeof(char),2, fp);
#ifdef _debug
        qDebug() << "echo " << "in >" << _directionString;
#endif
    }
    else
    if(direction == Out){
        fwrite("out", sizeof(char),3, fp);
#ifdef _debug
        qDebug() << "echo " << "out >" << _directionString;
#endif
    }

    _direction = direction;

    fclose(fp);

    return 0;
}
//------------------------------------------------------------------------------
GPIO::Direction GPIO::getDirection()
{
    return _direction;
}
//------------------------------------------------------------------------------
int GPIO::setState(bool state)
{
    //state is 0 or 1. No effect if other value.
    //in case all is ok, returns 0 otherwise  -1 on error

    FILE * fp;
    if ((fp = fopen(_valueString .toLatin1(), "rb+")) == NULL){
        return -1;
    }

    rewind(fp);//Set pointer to begining of the file

    if(state){
        fwrite("1", sizeof(char),1, fp);
#ifdef _debug
        qDebug() << "echo " << "1 >" << _valueString;
#endif
    }
    else{
        fwrite("0", sizeof(char), 1, fp);
#ifdef _debug
        qDebug() << "echo " << "0 >" << _valueString;
#endif
    }

    _state = state;

    fclose(fp);

    return 0;
}
//------------------------------------------------------------------------------
bool GPIO::getState()
{
    //
    // returns 1 or 0 - the pin state.
    // Returns -1 on error
    //

    FILE * fp;
    char value;

    if ((fp = fopen(_valueString.toLatin1(), "rb+")) == NULL) {
        qDebug() << "getState Error";
        return(false);
    }

    rewind(fp);//Set pointer to begining of the file
    fread(&value, sizeof(char),1, fp);
    fclose(fp);

    if(value=='1'){
        return true;
    }
    if(value=='0'){
        return false;
    }

    return false;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
TInputPinThread::TInputPinThread(GPIOManager* powner, int nms){

    pOwner = powner;
    nMs    = nms;

    _terminate = false;
    _priority  = QThread::NormalPriority;
    _isRunning = false;
}
//--------------------------------------------------------------------------------------------------
void TInputPinThread::Start(int ms){

    nMs = ms;

    if( _isRunning == false){
        _terminate = false;
        start(_priority);
    }
}
//--------------------------------------------------------------------------------------------------
void TInputPinThread::Stop(){
    _terminate = true;
    while(_isRunning);
}
//------------------------------------------------------------------------------
void TInputPinThread::run(){

    _isRunning = true;

    if(pOwner != NULL){

        while(_terminate == false) {

            pOwner->ckeckInputPin();

            msleep(nMs);
        }
    }

    _isRunning = false;

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
GPIOManager::GPIOManager(QObject* parent) : QObject(parent) {

    pInputThread = new TInputPinThread(this);
}
//------------------------------------------------------------------------------
GPIOManager::~GPIOManager(){

    if(pInputThread->isRunning())
        pInputThread->Stop();
    delete pInputThread;

    foreach(GPIO* p, gpio) {
        delete p;
    }
}
//------------------------------------------------------------------------------
bool GPIOManager::checkPortPin(int port, int pin){

    //ToDo
    return(true);
}
//------------------------------------------------------------------------------
bool GPIOManager::checkPortPin(int absolutePinNumber){

    //ToDo
    return(true);
}
//------------------------------------------------------------------------------
GPIO* GPIOManager::addPin(int port, int index, GPIO::Direction dir){

    if( checkPortPin(port,index)){

        int pinIdx = (((port)-1)*32)+((index)&31);

        if(gpioHashMap.contains(pinIdx) == false){

            GPIO* pGpio = new GPIO(port,index);

            if( pGpio != NULL ){

                if( pGpio->openPin() == 0 ){

                    if( pGpio->setDirection(dir) == 0 ){

                        gpio.append(pGpio);
                        oldGpioState.append(false);
                        gpioHashMap[pinIdx] = pGpio;

                        return(pGpio);
                    }
                }
            }
        }
    }

    return(NULL);
}

//------------------------------------------------------------------------------
void GPIOManager::ckeckInputPin(){

    for(int i=0;i<gpio.count();i++){

        GPIO* p = gpio.at(i);

        if(p->isInput()){

            bool stat = p->getState();
            if( stat != oldGpioState.at(i) ){
                oldGpioState[i] = stat;
                emit onInputPinStatusChanged(p,stat);
            }
        }
    }
}

//------------------------------------------------------------------------------
void GPIOManager::startService(int ms){

    pInputThread->Start(ms);
}

